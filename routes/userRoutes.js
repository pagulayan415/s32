// Create routes
const express = require('express')

// Router() handles the requests
const router = express.Router();

// syntax: router.HTTPmethod('uri', <request listener>)
router.get('/', (req, res) =>{
    // console.log('Hello from userRoutes')
    res.send('Hello from userRoutes')
})

module.exports = router;